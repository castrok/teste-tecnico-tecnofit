#release estável
FROM mysql:8.0.0

# Adicionando os scripts SQL para serem executados na criação do banco
COPY mysql_dump.sql /docker-entrypoint-initdb.d/mysql_dump.sql

#Abilita conexões externas a rede do docker
COPY mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf

EXPOSE 3306
