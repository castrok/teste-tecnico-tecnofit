<?php

namespace App\Views;
use Illuminate\Database\Eloquent\Model;

class Ranking extends Model {

    protected $table = 'ranking';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'movement_name'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date' => 'date:d/m/Y H:i'
    ];
}
