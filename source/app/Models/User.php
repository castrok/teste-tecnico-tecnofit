<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class User extends Model {

//    public $timestamps = false;
//    protected $primaryKey = null;
//    public $incrementing = false;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
//        'id',
//        'name',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [  ];


    public function personalRecord() {
        return $this->hasMany( \App\Models\PersonalRecord::class, 'user_id', 'id');
    }

}
