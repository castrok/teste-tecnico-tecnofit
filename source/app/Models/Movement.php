<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Movement extends Model {

    protected $table = 'movement';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
//        'id',
//        'name',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [  ];

    public function personalRecord() {
        return $this->hasMany( \App\Models\PersonalRecord::class, 'movement_id', 'id');
    }

    public function roles() {
        return $this->belongsToMany(\App\Models\User::class, 'personal_record', 'user_id', 'movement_id')->with('personalRecord');
    }
}
