<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PersonalRecord extends Model {

    protected $table = 'personal_record';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
//        'id',
//        'user_id',
//        'movement_id',
//        'value',
//        'date',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'user_id',
        'movement_id',
        'pivot'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date' => 'date:d/m/Y H:i'
    ];

    public function user() {
        return $this->hasOne( \App\Models\User::class, 'id', 'user_id');
    }

    public function movement() {
        return $this->hasOne( \App\Models\Movement::class, 'id', 'movement_id');
    }
}
