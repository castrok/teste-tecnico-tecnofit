<?php

use App\Models\PersonalRecord;
use App\Models\User;
use App\Views\Ranking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('index/{id}', function (string $id) {

    $ranking = Ranking::where('id', $id)->get();

    foreach ($ranking as $index => $rank) {
        $ranking[$index]['date'] = PersonalRecord::where([
            ['movement_id', $id ],
            ['user_id', $rank->userID ],
            ['value', $rank->nota ],
        ])->first()->toArray()['date'];
    }

    $response = $ranking->makeHidden('userID')->groupBy('movement_name');
    Return Response::json($response);
});
