FROM nginx:stable
MAINTAINER 'Futurefy Systems'
RUN apt update -y

WORKDIR /var/www/html/
ADD default.conf /etc/nginx/conf.d/
#ADD .htpasswd /etc/nginx/
